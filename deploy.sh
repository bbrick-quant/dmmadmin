
REMOTE_HOST=ubuntu@3.35.247.111
CERT_FILE=bbrick-aws1.pem
PKG=dmmadmin.tar
APP_DIR=/home/ubuntu/work
SHELL_DIR=/home/ubuntu/work/shell/dmmadmin

cd ..
tar cvf $PKG ./dmmadmin/
scp -i ~/.ssh/$CERT_FILE ./$PKG $REMOTE_HOST:$APP_DIR

ssh -t -i ~/.ssh/$CERT_FILE -t $REMOTE_HOST "cd $SHELL_DIR; ./dmm_admin_server_run.sh;"
