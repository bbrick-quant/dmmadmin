from rest_framework import generics, permissions
from rest_framework import mixins
from rest_framework.response import Response

import csv
from django.http import HttpResponse
from django.utils import dateformat, timezone, dateparse
import datetime
from time import sleep


from .models import (
    SwapHistory,
    ExBalance,
    ExQuotePairCtrl,
    AutoRebalanceConf,
    RebalanceReqQueue,
    CtrlReqQueue,
)
from .serializers import (
    DashboardSwapHistorySerializer,
    ExBalanceSerializer,
    ReportSwapHistorySerializer,
    QuoteListSerializer,
    QuoteCreateSerializer,
    RebalanceListSerializer,
    ReqRebalanceCreateSerializer,
    RebalanceHistorySerializer,
)

from django.db.models import Sum


class ExBalanceView(generics.ListAPIView):
    # TODO: 권한 처리
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = ExBalanceSerializer

    def get_queryset(self):
        return ExBalance.objects.filter(ex_name='coinone').all()


class SwapHistoryView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = DashboardSwapHistorySerializer

    def get_queryset(self):
        return SwapHistory.objects.all().order_by('-ex_time_str')[:10]


class RebalanceHistoryView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = RebalanceHistorySerializer

    def get_queryset(self):
        start = self.request.GET.get('start')
        end = self.request.GET.get('end')

        return RebalanceReqQueue.objects.filter(created_at__range=(start, end)).all().order_by('-created_at')


class DashboardRebalanceHistoryView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = RebalanceHistorySerializer

    def get_queryset(self):
        return RebalanceReqQueue.objects.all().order_by('-updated_at')[:10]


class VolumeForDashboardView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        res = {}
        today = datetime.date.today()
        start = request.GET.get('start')
        end = request.GET.get('end')

        today_volume = SwapHistory.objects.filter(created_at__date=today).aggregate(Sum('swap_volume'))
        today_profit = SwapHistory.objects.filter(created_at__date=today).aggregate(Sum('expected_pnl'))

        month_volume = SwapHistory.objects.filter(created_at__range=(start, end)).aggregate(Sum('swap_volume'))
        month_profit = SwapHistory.objects.filter(created_at__range=(start, end)).aggregate(Sum('expected_pnl'))

        res['today_volume'] = today_volume['swap_volume__sum']
        res['today_profit'] = today_profit['expected_pnl__sum']
        res['month_volume'] = month_volume['swap_volume__sum']
        res['month_profit'] = month_profit['expected_pnl__sum']

        return Response(res)


class VolumeForChartView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        start = request.GET.get('start')
        end = request.GET.get('end')

        month_volume = SwapHistory.objects \
            .filter(created_at__range=(start, end)) \
            .extra(select={'created_at': "TO_CHAR(created_at AT TIME ZONE 'Asia/Seoul', 'YYYY-MM-DD')"}) \
            .values('created_at') \
            .annotate(volume=Sum('swap_volume'), pnl=Sum('expected_pnl')).order_by('created_at')

        return Response(month_volume)


class ReportSwapHistoryView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = ReportSwapHistorySerializer

    def get_queryset(self):
        start = self.request.GET.get('start')
        end = self.request.GET.get('end')
        pair = self.request.GET.get('pair')

        if pair:
            return SwapHistory.objects.filter(ex_time_str__range=(start, end), swap_pair=pair).all().order_by('-ex_time_str')
        return SwapHistory.objects.filter(ex_time_str__range=(start, end)).all().order_by('-ex_time_str')


class ReportSwapHistoryDownloadView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = ReportSwapHistorySerializer

    def get(self, request, *args, **kwargs):
        start = self.request.GET.get('start')
        end = self.request.GET.get('end')
        pair = self.request.GET.get('pair')

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="swap_history_{dateformat.format(timezone.now(), "Y-m-d H:i:s")}.csv"'

        if pair:
            serializer = self.get_serializer(
                SwapHistory.objects.filter(ex_time_str__range=(start, end), swap_pair=pair).all().order_by('ex_time_str'),
                many=True
            )
        else:
            serializer = self.get_serializer(
                SwapHistory.objects.filter(ex_time_str__range=(start, end)).all().order_by('ex_time_str'),
                many=True
            )

        header = ReportSwapHistorySerializer.Meta.fields
        writer = csv.DictWriter(response, fieldnames=header)
        writer.writeheader()
        for row in serializer.data:
            writer.writerow(row)

        return response


class CSVRebalanceHistoryDownloadView(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = RebalanceHistorySerializer

    def get(self, request, *args, **kwargs):
        start = self.request.GET.get('start')
        end = self.request.GET.get('end')

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = f'attachment; filename="rebalance_history_{dateformat.format(timezone.now(), "Y-m-d H:i:s")}.csv"'

        serializer = self.get_serializer(
            RebalanceReqQueue.objects.filter(created_at__range=(start, end)).all().order_by('created_at'),
            many=True
        )

        header = RebalanceHistorySerializer.Meta.fields
        writer = csv.DictWriter(response, fieldnames=header)
        writer.writeheader()
        for row in serializer.data:
            writer.writerow(row)

        return response


class QuoteListView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = QuoteListSerializer

    def get_queryset(self):
        return ExQuotePairCtrl.objects.all().order_by('-id')


class ReqQuoteDetailView(generics.GenericAPIView, mixins.RetrieveModelMixin):
    # permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        res = {}
        obj = CtrlReqQueue.objects.filter(id=kwargs.get('pk')).all().values()[0]

        updated = obj['updated_at']

        if updated == None:
            for _ in range(0, 6):
                tmp_obj = CtrlReqQueue.objects.filter(id=kwargs.get('pk')).all().values()[0]

                if tmp_obj["updated_at"] != None:
                    res['detail_info'] = tmp_obj
                    return Response(res)

                sleep(1)


        res['detail_info'] = obj
        return Response(res)


class QuoteCreateView(generics.ListCreateAPIView):
    # permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return QuoteCreateSerializer


class ReqRebalanceCreateView(generics.ListCreateAPIView):
    # permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return ReqRebalanceCreateSerializer


class RebalanceListView(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated]
    serializer_class = RebalanceListSerializer

    def get_queryset(self):
        return AutoRebalanceConf.objects.all().order_by('-threshhold_min')


class AutoRebalanceUpdateView(generics.RetrieveUpdateAPIView):
    # permission_classes = [permissions.IsAuthenticated]

    def patch(self, request, *args, **kwargs):
        params = request.data
        ex_name = params['ex_name']
        coin = params['coin']
        target = {}

        for key in params:
            if 'status' == key:
                target[key] = params[key]
            if 'threshhold_min' == key:
                target[key] = params[key]
            if 'target_compare_percent' == key:
                target[key] = params[key]

        target['updated_at'] = timezone.now()
        instance = AutoRebalanceConf.objects.filter(ex_name=ex_name, coin=coin).update(**target)

        return Response(instance)

