from django.contrib import admin
from .models import ExBalance, ExBalanceHistory, SwapHistory
# Register your models here.

admin.site.register(SwapHistory)
admin.site.register(ExBalance)
admin.site.register(ExBalanceHistory)
