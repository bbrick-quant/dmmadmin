# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AutoRebalanceConf(models.Model):
    ex_name = models.CharField(primary_key=True, max_length=16)
    coin = models.CharField(max_length=8)
    status = models.CharField(max_length=4)
    threshhold_min = models.DecimalField(max_digits=20, decimal_places=8)
    target_compare_percent = models.DecimalField(max_digits=8, decimal_places=4)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'auto_rebalance_conf'
        unique_together = (('ex_name', 'coin'),)


class CtrlReqQueue(models.Model):
    id = models.BigAutoField(primary_key=True)
    ex_name = models.CharField(max_length=16, default='coinone')
    ctrl_name = models.CharField(max_length=16, default='coinone_dmm')
    req_data = models.JSONField(blank=True, null=True)
    handle_status = models.CharField(max_length=1, blank=True, null=True, default='R')
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_name = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = 'ctrl_req_queue'


class ExBalance(models.Model):
    ex_name = models.CharField(primary_key=True, max_length=16)
    coin = models.CharField(max_length=8)
    avail = models.DecimalField(max_digits=20, decimal_places=8)
    balance = models.DecimalField(max_digits=20, decimal_places=8)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ex_balance'
        unique_together = (('ex_name', 'coin'),)


class ExBalanceHistory(models.Model):
    id = models.BigAutoField(primary_key=True)
    ex_name = models.CharField(max_length=16)
    balances = models.JSONField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ex_balance_history'


class ExQuotePairCtrl(models.Model):
    ex_name = models.CharField(max_length=16)
    swap_pair = models.CharField(max_length=16)
    quote_aggressiveness = models.DecimalField(max_digits=16, decimal_places=8)
    bid_percent = models.DecimalField(max_digits=8, decimal_places=4)
    ask_percent = models.DecimalField(max_digits=8, decimal_places=4)
    status = models.CharField(max_length=4)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ex_quote_pair_ctrl'
        unique_together = (('ex_name', 'swap_pair'),)


class RebalanceOrder(models.Model):
    id = models.BigAutoField(primary_key=True)
    rebalance_req_queue = models.ForeignKey('RebalanceReqQueue', models.DO_NOTHING, blank=True, null=True)
    ex_name = models.CharField(max_length=16)
    client_order_id = models.CharField(max_length=64)
    exchange_order_id = models.CharField(max_length=64)
    type = models.CharField(max_length=8)
    pair = models.CharField(max_length=16)
    target_price = models.DecimalField(max_digits=20, decimal_places=8)
    target_quantity = models.DecimalField(max_digits=20, decimal_places=8)
    average_filled_price = models.DecimalField(max_digits=20, decimal_places=8)
    filled_quantity = models.DecimalField(max_digits=20, decimal_places=8)
    fee = models.DecimalField(max_digits=20, decimal_places=8)
    fee_asset = models.CharField(max_length=8)
    plan_order = models.SmallIntegerField()
    status = models.CharField(max_length=1)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'rebalance_order'


class RebalancePlan(models.Model):
    id = models.BigAutoField(primary_key=True)
    ctrl_name = models.CharField(max_length=16)
    plan_ex_name = models.CharField(max_length=16)
    plan = models.CharField(max_length=16)
    target_coin = models.CharField(max_length=8)
    base_market = models.CharField(max_length=8, blank=True, null=True)
    coin_over = models.CharField(max_length=8)
    coin_lack = models.CharField(max_length=8)
    plan_order = models.SmallIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rebalance_plan'
        unique_together = (('coin_over', 'coin_lack', 'plan_order'),)


class RebalanceReqQueue(models.Model):
    id = models.BigAutoField(primary_key=True)
    ctrl_name = models.CharField(max_length=16, default='coinone_dmm')
    ex_name = models.CharField(max_length=16, default='coinone')
    from_coin = models.CharField(max_length=8)
    to_coin = models.CharField(max_length=8)
    from_amount = models.DecimalField(max_digits=20, decimal_places=8)
    to_amount = models.DecimalField(max_digits=20, decimal_places=8, default=0, blank=True, null=True)
    handle_status = models.CharField(max_length=1, default='R')
    completed_plan = models.SmallIntegerField(default=0, blank=True, null=True)
    user_name = models.CharField(max_length=32)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True)

    class Meta:
        managed = False
        db_table = 'rebalance_req_queue'


class RebalanceTransfer(models.Model):
    id = models.BigAutoField(primary_key=True)
    rebalance_req_queue = models.ForeignKey(RebalanceReqQueue, models.DO_NOTHING, blank=True, null=True)
    ex_name = models.CharField(max_length=16)
    to_address = models.CharField(max_length=128)
    to_ex_name = models.CharField(max_length=16)
    amount = models.DecimalField(max_digits=20, decimal_places=8)
    tx_id = models.CharField(max_length=128)
    ex_fee = models.DecimalField(max_digits=20, decimal_places=8)
    plan_order = models.SmallIntegerField()
    status = models.CharField(max_length=1)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rebalance_transfer'


class SwapHistory(models.Model):
    id = models.BigAutoField(primary_key=True)
    ex_name = models.CharField(max_length=16)
    ex_swap_id = models.CharField(max_length=64)
    ex_time_str = models.CharField(max_length=16, blank=True, null=True)
    price = models.DecimalField(max_digits=20, decimal_places=8)
    expected_pnl = models.DecimalField(max_digits=20, decimal_places=8)
    from_name = models.CharField(max_length=8)
    from_amount = models.DecimalField(max_digits=20, decimal_places=8)
    to_name = models.CharField(max_length=8)
    to_amount = models.DecimalField(max_digits=20, decimal_places=8)
    swap_pair = models.CharField(max_length=16)
    swap_volume = models.DecimalField(max_digits=20, decimal_places=8)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'swap_history'
        unique_together = (('ex_name', 'ex_swap_id'),)
