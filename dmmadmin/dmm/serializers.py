from rest_framework import serializers
from datetime import datetime
from django.db import transaction

from .models import (
    SwapHistory,
    ExBalance,
    ExQuotePairCtrl,
    CtrlReqQueue,
    AutoRebalanceConf,
    RebalanceReqQueue,
)


class ExBalanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExBalance
        fields = ('ex_name', 'coin', 'balance', 'updated_at',)


class DashboardSwapHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SwapHistory
        fields = ('id', 'from_name', 'from_amount', 'to_name', 'to_amount', 'expected_pnl', 'ex_time_str',)


class RebalanceHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = RebalanceReqQueue
        fields = ('id', 'ctrl_name', 'ex_name', 'from_coin', 'to_coin', 'from_amount', 'to_amount', 'handle_status', 'completed_plan', 'user_name', 'created_at', 'updated_at',)


class ReportSwapHistorySerializer(serializers.ModelSerializer):
    ex_time_str = serializers.SerializerMethodField()

    class Meta:
        model = SwapHistory
        fields = ('ex_swap_id', 'price', 'from_name', 'from_amount', 'to_name', 'to_amount', 'swap_pair', 'expected_pnl', 'swap_volume', 'ex_time_str',)

    def get_ex_time_str(self, obj):
        return datetime.fromtimestamp(int(obj.ex_time_str))


class QuoteCreateSerializer(serializers.ModelSerializer):
    @transaction.atomic
    def create(self, validated_data):
        req_data = validated_data['req_data']
        user_name = validated_data['user_name']

        req_queue = CtrlReqQueue(
            req_data=req_data,
            user_name=user_name
        )
        req_queue.save()

        return req_queue

    class Meta:
        model = CtrlReqQueue
        fields = ('id', 'req_data', 'user_name', 'handle_status', 'created_at', 'updated_at',)


class ReqRebalanceCreateSerializer(serializers.ModelSerializer):
    @transaction.atomic
    def create(self, validated_data):
        from_coin = validated_data['from_coin']
        to_coin = validated_data['to_coin']
        from_amount = validated_data['from_amount']
        user_name = validated_data['user_name']

        req_queue = RebalanceReqQueue(
            from_coin=from_coin,
            to_coin=to_coin,
            from_amount=from_amount,
            user_name=user_name
        )
        req_queue.save()
        return req_queue

    class Meta:
        model = RebalanceReqQueue
        fields = ('from_coin', 'to_coin', 'from_amount', 'user_name',)


class QuoteListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExQuotePairCtrl
        fields = ('ex_name', 'status', 'swap_pair', 'bid_percent', 'ask_percent', 'updated_at',)


class ReqQuoteQueueSerializer(serializers.ModelSerializer):
    class Meta:
        model = CtrlReqQueue
        fields = ('id', 'handle_status', 'created_at', 'updated_at',)


class RebalanceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoRebalanceConf
        fields = ('ex_name', 'coin', 'status', 'threshhold_min', 'target_compare_percent', 'updated_at',)
