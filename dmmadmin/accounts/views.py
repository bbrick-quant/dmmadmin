from rest_framework import generics, permissions

from .serializers import FundSerializer, FundBalanceSnapshotHistorySerializer

from .models import Fund

import logging

logger = logging.getLogger(__name__)

class FundListView(generics.ListAPIView):

    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FundSerializer

    def get_queryset(self):
        return Fund.objects.using('mothership').filter(user_id=1)


class FundDetailView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FundSerializer
    lookup_field = 'id'

    def get_queryset(self):
        id = self.kwargs['id']
        return Fund.objects.using('mothership').filter(id=id)


class FundBalanceSnapshotHistoryDetailView(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = FundBalanceSnapshotHistorySerializer
    lookup_field = 'id'

    def get_queryset(self):
        id = self.kwargs['id']
        return Fund.objects.using('mothership').filter(id=id)
