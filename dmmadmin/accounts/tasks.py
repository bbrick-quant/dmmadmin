from celery import shared_task
from .models import BalanceSnapshot, ApiKey
from legacy.models import Balance
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)


@shared_task
def sync_balance(account_id_api_key_id_map: dict) -> None:
    for account_id, api_key_id in account_id_api_key_id_map.items():
        api_key = ApiKey.objects.using('mothership').get(id=api_key_id)
        legacy_balance = Balance.objects.using('mothership').filter(account_id=account_id).order_by('-created_at')[0]
        balance_snapshot = BalanceSnapshot(assets={
            currency.upper(): {'available': b[0], 'locked': b[1]} for currency, b in legacy_balance.assets.items()
        }, api_key=api_key)
        balance_snapshot.save()
