from django.contrib import admin
from .models import Fund, Account, ApiKey, BalanceSnapshot


class FundAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    list_display = ('name', 'initial_balance',)
    search_fields = ('name',)


class AccountAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    list_display = ('name', 'description', 'exchange')
    search_fields = ('name', 'exchange',)


class ApiKeyAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    list_display = (
        'api_key', 'maker_fee', 'taker_fee',
        'instrument_categories',
    )
    search_fields = ('api_key', 'instrument_categories',)


class BalanceSnapshotAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
    list_display = ('id', 'api_key', 'raw_response', 'created_at',)


admin.site.register(Fund, FundAdmin)
admin.site.register(Account, AccountAdmin)
admin.site.register(ApiKey, ApiKeyAdmin)
admin.site.register(BalanceSnapshot, BalanceSnapshotAdmin)
