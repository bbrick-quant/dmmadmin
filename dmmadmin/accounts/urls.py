from django.urls import path
from .views import FundListView, FundDetailView, FundBalanceSnapshotHistoryDetailView

urlpatterns = [
    path('', FundListView.as_view()),
    path('<int:id>/', FundDetailView.as_view()),
    path('<int:id>/balance-snapshots/', FundBalanceSnapshotHistoryDetailView.as_view()),
]
