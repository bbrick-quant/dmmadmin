from django.contrib.postgres.fields import ArrayField
from django.db import models

# from users.models import User


class Fund(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    initial_balance = models.JSONField(default=dict)
    debt_balance = models.JSONField(default=dict)
    # TODO: Fund duration and settlement
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user_id = models.IntegerField()

    # user = models.ForeignKey(
    #     User,
    #     on_delete=models.CASCADE,
    #     related_name='funds'
    # )

    class Meta:
        db_table = "funds"


class Account(models.Model):
    class Category(models.TextChoices):
        MAIN = 'main'
        SUB = 'sub'

    class Exchange(models.TextChoices):
        # TODO: Add more exchanges!
        BITHUMB = 'bithumb'
        HUOBI = 'huobi'
        COINONE = 'coinone'
        FTX = 'ftx'
        GDAC = 'gdac'
        BINANCE = 'binance'

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    exchange_account_id = models.CharField(max_length=255, blank=True)
    category = models.CharField(max_length=50, choices=Category.choices, default=Category.MAIN)
    exchange = models.CharField(max_length=50, choices=Exchange.choices)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    fund = models.ForeignKey(
        Fund,
        on_delete=models.CASCADE,
        related_name='accounts'
    )

    def trade_api_key(self):
        return self.api_keys.filter(permissions=[ApiKey.Permission.TRADE.value]).first()

    def transfer_api_key(self):
        return self.api_keys.filter(permissions=[ApiKey.Permission.TRANSFER.value]).first()

    def __str__(self):
        return f'{self.id} - {self.name} - {self.description}'

    class Meta:
        db_table = "accounts"


class ApiKey(models.Model):
    class InstrumentCategory(models.TextChoices):
        SPOT = 'spot'
        FUTURES = 'futures'
        PERPETUAL_FUTURES = 'perpetual_futures'
        OPTIONS = 'options'

    class Permission(models.TextChoices):
        READ = 'read'
        TRADE = 'trade'
        TRANSFER = 'transfer'

    api_key = models.CharField(max_length=255)
    api_secret = models.CharField(max_length=255)
    maker_fee = models.DecimalField(max_digits=21, decimal_places=8)
    taker_fee = models.DecimalField(max_digits=21, decimal_places=8)
    permissions = ArrayField(models.CharField(max_length=50, choices=Permission.choices))
    available_ips = ArrayField(models.GenericIPAddressField())
    instrument_categories = ArrayField(models.CharField(max_length=50, choices=InstrumentCategory.choices))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    account = models.ForeignKey(
        Account,
        on_delete=models.CASCADE,
        related_name='api_keys'
    )

    def balance_snapshot(self):
        return BalanceSnapshot.objects.using('mothership').filter(api_key=self).order_by('-created_at')[0]

    class Meta:
        db_table = "api_keys"


class BalanceSnapshot(models.Model):
    assets = models.JSONField()
    raw_response = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    api_key = models.ForeignKey(
        ApiKey,
        on_delete=models.CASCADE,
        related_name='balance_snapshots'
    )

    class Meta:
        db_table = "balance_snapshots"
        indexes = [
            models.Index(fields=['api_key_id', '-created_at']),
            models.Index(fields=['created_at']),
        ]
