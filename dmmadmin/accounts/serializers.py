from rest_framework import serializers

from .models import Fund, Account, ApiKey, BalanceSnapshot


class BalanceSnapshotSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceSnapshot
        fields = ('id', 'assets', 'raw_response', 'created_at',)


class ApiKeySerializer(serializers.ModelSerializer):
    balance_snapshot = BalanceSnapshotSerializer(read_only=True)

    class Meta:
        model = ApiKey
        fields = (
            'id', 'api_key', 'maker_fee', 'taker_fee',
            'permissions', 'available_ips', 'instrument_categories', 'balance_snapshot',
        )


class AccountSerializer(serializers.ModelSerializer):
    api_keys = ApiKeySerializer(many=True, read_only=True)

    class Meta:
        model = Account
        fields = ('id', 'name', 'description', 'exchange_account_id', 'category', 'exchange', 'api_keys',)


class FundSerializer(serializers.ModelSerializer):
    accounts = AccountSerializer(many=True, read_only=True)

    class Meta:
        model = Fund
        fields = ('id', 'name', 'description', 'initial_balance', 'debt_balance', 'accounts')


class BalanceSnapshotHistorySerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        date = self.context.get('request').query_params.get('date', None)
        if date:
            try:
                return BalanceSnapshotSerializer(
                    BalanceSnapshot.objects.using('mothership').filter(
                        api_key_id=instance.api_key_id,
                        created_at__date=date).earliest('created_at')
                ).data
            except BalanceSnapshot.DoesNotExist:
                return None
        return BalanceSnapshotSerializer(instance).data

    class Meta:
        model = BalanceSnapshot
        fields = ('id', 'assets', 'raw_response', 'created_at',)


class ApiKeyBalanceSnapshotHistorySerializer(serializers.ModelSerializer):
    balance_snapshot = BalanceSnapshotHistorySerializer(read_only=True)

    class Meta:
        model = ApiKey
        fields = (
            'id', 'api_key', 'maker_fee', 'taker_fee',
            'permissions', 'available_ips', 'instrument_categories', 'balance_snapshot',
        )


class AccountBalanceSnapshotHistorySerializer(serializers.ModelSerializer):
    api_keys = ApiKeyBalanceSnapshotHistorySerializer(many=True, read_only=True)

    class Meta:
        model = Account
        fields = ('id', 'name', 'description', 'exchange_account_id', 'category', 'exchange', 'api_keys',)


class FundBalanceSnapshotHistorySerializer(serializers.ModelSerializer):
    accounts = AccountBalanceSnapshotHistorySerializer(many=True, read_only=True)

    class Meta:
        model = Fund
        fields = ('id', 'name', 'description', 'initial_balance', 'debt_balance', 'accounts',)
