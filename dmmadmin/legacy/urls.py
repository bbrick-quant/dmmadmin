from django.urls import path
from .views import (
    MarketListView,
    PairListView,
    PairMarketListView,
)

urlpatterns = [
    path('', MarketListView.as_view()),
    path('pairs/', PairMarketListView.as_view()),
    path('<int:id>/pairs/', PairListView.as_view()),
]
