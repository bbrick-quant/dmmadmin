from rest_framework import serializers

from .models import Market, Pair


class MarketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Market
        fields = ('id', 'exchange', 'instrument',)


class PairSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pair
        fields = (
            'id', 'lot_size', 'tick_size',
            'instrument_type', 'internal_ticker', 'external_ticker',
            'market_id'
        )


class PairMarketSerializer(serializers.ModelSerializer):
    market = MarketSerializer(read_only=True)

    class Meta:
        model = Pair
        fields = (
            'id', 'lot_size', 'tick_size',
            'instrument_type', 'internal_ticker', 'external_ticker',
            'market'
        )
