from django.db import models


class Market(models.Model):
    exchange = models.CharField(max_length=20, blank=True, null=True)
    instrument = models.CharField(max_length=20, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'market'


class Currency(models.Model):
    market = models.ForeignKey(Market, models.DO_NOTHING, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    external_currency = models.CharField(max_length=20, blank=True, null=True)
    internal_currency = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'currency'


class Pair(models.Model):
    lot_size = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    tick_size = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    state = models.CharField(max_length=20, blank=True, null=True)
    instrument_type = models.CharField(max_length=20, blank=True, null=True)
    base_min = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    quote_min = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    market = models.ForeignKey(Market, models.DO_NOTHING, blank=True, null=True)
    base_currency = models.ForeignKey(Currency, models.DO_NOTHING, blank=True, null=True, related_name='base_currency')
    quote_currency = models.ForeignKey(Currency, models.DO_NOTHING, blank=True, null=True, related_name='quote_currency')
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    multiplier = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    external_ticker = models.CharField(max_length=20, blank=True, null=True)
    internal_ticker = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pair'


class Order(models.Model):
    exchange_order_id = models.CharField(max_length=200, blank=True, null=True)
    client_order_id = models.CharField(max_length=200, blank=True, null=True)
    order_group_id = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=100, blank=True, null=True)
    base_currency_id = models.IntegerField(blank=True, null=True)
    quote_currency_id = models.IntegerField(blank=True, null=True)
    price = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    quantity = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    filled_quantity = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    average_filled_price = models.DecimalField(max_digits=24, decimal_places=8, blank=True, null=True)
    side = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=100, blank=True, null=True)
    execution_type = models.CharField(max_length=100, blank=True, null=True)
    timeout = models.FloatField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    market_id = models.IntegerField(blank=True, null=True)
    pair_id = models.IntegerField(blank=True, null=True)
    exchange_created_at = models.DateTimeField(blank=True, null=True)
    exchange_updated_at = models.DateTimeField(blank=True, null=True)
    kind = models.CharField(max_length=100, blank=True, null=True)
    leverage = models.FloatField(blank=True, null=True)
    margin_type = models.CharField(max_length=100, blank=True, null=True)
    offset_direction = models.CharField(max_length=100, blank=True, null=True)
    order_metadata = models.JSONField(blank=True, null=True)
    raw_response = models.TextField(blank=True, null=True)
    reject_reason = models.TextField(blank=True, null=True)
    strategy_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'order'


class Balance(models.Model):
    market_id = models.IntegerField()
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    account_id = models.IntegerField(blank=True, null=True)
    assets = models.JSONField(blank=True, null=True)
    balance_metadata = models.JSONField(blank=True, null=True)
    raw_response = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'balance'


class Position(models.Model):
    market_id = models.IntegerField()
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    account_id = models.IntegerField(blank=True, null=True)
    position_metadata = models.JSONField(blank=True, null=True)
    raw_response = models.TextField(blank=True, null=True)
    contracts = models.JSONField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'position'


class Strategy(models.Model):
    key = models.CharField(max_length=40, blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    state = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'strategy'
