from rest_framework import generics, permissions

from .models import Market, Pair
from .serializers import MarketSerializer, PairSerializer, PairMarketSerializer


class MarketListView(generics.ListAPIView):
    permission_classes = [permissions.AllowAny]
    queryset = Market.objects.using('legacy').filter(instrument='spot')
    serializer_class = MarketSerializer


class PairListView(generics.ListAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = PairSerializer

    def get_queryset(self):
        return Pair.objects.using('legacy').filter(market_id=self.kwargs.get('id'))


class PairMarketListView(generics.ListAPIView):
    permission_classes = [permissions.AllowAny]
    serializer_class = PairMarketSerializer
    queryset = Pair.objects.using('legacy').all()
